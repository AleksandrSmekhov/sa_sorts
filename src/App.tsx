import { Suspense } from 'react';

import { Content, LoadingElement } from './components';

const App = () => {
  return (
    <Suspense fallback={<LoadingElement />}>
      <Content />
    </Suspense>
  );
};

export default App;
