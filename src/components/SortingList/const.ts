import type { ISort } from './ui/SortingList.interface';

export enum SortNames {
  bubble = 'bubbleSort',
  mixing = 'mixingSort',
  hairbrush = 'hairbrushSort',
  insert = 'insertSort',
  choice = 'choiceSort',
  fast = 'fastSort',
  merge = 'mergeSort',
  pyramid = 'pyramidSort',
}

export const SORTS_LIST: ISort[] = [
  { key: 'bubble-sort-key', sortName: SortNames.bubble },
  { key: 'mixing-sort-key', sortName: SortNames.mixing },
  { key: 'hairbrush-sort-key', sortName: SortNames.hairbrush },
  { key: 'insert-sort-key', sortName: SortNames.insert },
  { key: 'choice-sort-key', sortName: SortNames.choice },
  { key: 'fast-sort-key', sortName: SortNames.fast },
  { key: 'merge-sort-key', sortName: SortNames.merge },
  { key: 'pyramid-sort-key', sortName: SortNames.pyramid },
];
