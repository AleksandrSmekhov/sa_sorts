import { SortNames } from '../const';

export interface ISort {
  key: string;
  sortName: SortNames;
}
