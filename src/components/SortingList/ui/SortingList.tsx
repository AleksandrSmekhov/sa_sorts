import { SortingItem } from 'components';

import { SORTS_LIST } from '../const';

import cls from './SortingList.module.scss';

export const SortingList = () => {
  return (
    <div className={cls.sorts}>
      {SORTS_LIST.map((sort, index) => (
        <SortingItem
          key={sort.key}
          sortName={sort.sortName}
          index={index + 1}
        />
      ))}
    </div>
  );
};
