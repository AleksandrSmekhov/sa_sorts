import type { IPropertiesValues } from './ui/PropertiesTab.interface';

export const PROPERTIES_VALUES: IPropertiesValues[] = [
  {
    fieldName: 'bestTime',
    key: 'bestTime-property-info',
    title: 'Лучшее время:',
  },
  {
    fieldName: 'meanTime',
    key: 'meanTime-property-info',
    title: 'Среднее время:',
  },
  {
    fieldName: 'worstTime',
    key: 'worstTime-property-info',
    title: 'Худшее время:',
  },
  {
    fieldName: 'memory',
    key: 'memory-property-info',
    title: 'Затраты по памяти:',
  },
];
