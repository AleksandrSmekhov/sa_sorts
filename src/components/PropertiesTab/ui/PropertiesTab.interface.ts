import type { ISortValues } from 'components/SortingItem/ui/SortingItem.interface';

export interface IPropertiesTabProps {
  sortValues: ISortValues;
}

export interface IPropertiesValues {
  key: string;
  title: string;
  fieldName: keyof ISortValues;
}
