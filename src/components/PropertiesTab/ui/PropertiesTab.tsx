import type { IPropertiesTabProps } from './PropertiesTab.interface';

import { PROPERTIES_VALUES } from '../const';

import cls from './PropertiesTab.module.scss';

export const PropertiesTab = ({ sortValues }: IPropertiesTabProps) => {
  return (
    <div className={cls.properties}>
      {PROPERTIES_VALUES.map((values) =>
        sortValues[values.fieldName] ? (
          <div key={values.key} className={cls.propertyLine}>
            <p>{values.title}</p>
            <p>О({sortValues[values.fieldName]})</p>
          </div>
        ) : (
          ''
        )
      )}
    </div>
  );
};
