import { ReactComponent as GitLab } from 'icons/gitlab.svg';

import cls from './Header.module.scss';

export const Header = () => {
  return (
    <div className={cls.header}>
      <a href="https://smekhov-alex.ru" className={cls.backButton}>
        ← На главную
      </a>
      <div className={cls.headerContent}>
        <h1 className={cls.title}>Сортировки</h1>
        <a
          className={cls.sourceCode}
          href="https://gitlab.com/AleksandrSmekhov/sa_sorts"
          target="_black"
        >
          <GitLab /> Исходный код
        </a>
      </div>
    </div>
  );
};
