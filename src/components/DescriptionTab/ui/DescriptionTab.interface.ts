export interface IDescriptionTabProps {
  gifPath: string;
  title: string;
  description: string;
}
