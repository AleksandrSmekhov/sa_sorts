import type { IDescriptionTabProps } from './DescriptionTab.interface';

import cls from './DescriptionTab.module.scss';

export const DescriptionTab = ({
  description,
  gifPath,
  title,
}: IDescriptionTabProps) => {
  return (
    <div className={cls.descriptionTab}>
      <img src={gifPath} alt={title} className={cls.descriptionImg} />
      <div className={cls.description}>
        {description.split('\n').map((line, index) => (
          <p key={`description-line-${index}`}>{line}</p>
        ))}
      </div>
    </div>
  );
};
