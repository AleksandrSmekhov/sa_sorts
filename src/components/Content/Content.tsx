import { Header, SortingList } from 'components';

import cls from './Content.module.scss';

export const Content = () => {
  return (
    <div className={cls.content}>
      <Header />
      <SortingList />
    </div>
  );
};
