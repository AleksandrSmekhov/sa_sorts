import { SortNames } from 'components/SortingList/const';

export interface ISortingItemProps {
  sortName: SortNames;
  index: number;
}

export interface ISortValues {
  title: string;
  worstTime: string;
  meanTime: string;
  bestTime: string;
  memory: string;
  gifPath: string;
  description: string;
}

export type ISortsValues = Record<SortNames, ISortValues>;

export interface ITabValues {
  number: 0 | 1 | 2;
  title: string;
  key: string;
}

export type ITabs = Record<0 | 1 | 2, JSX.Element>;
