import { useMemo, useState } from 'react';

import { ReactComponent as Arrow } from 'icons/arrow.svg';
import { SortingTab, DescriptionTab, PropertiesTab } from 'components';

import type { ISortingItemProps, ITabs } from './SortingItem.interface';

import { SORTS_VALUES, TABS_VALUES } from '../const';

import cls from './SortingItem.module.scss';

export const SortingItem = ({ sortName, index }: ISortingItemProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [activeTab, setActiveTab] = useState<0 | 1 | 2>(0);

  const handleOpen = () => {
    setIsOpen((prev) => !prev);
  };

  const handleChangeTab = (newTab: 0 | 1 | 2) => {
    setActiveTab(newTab);
  };

  const tabs = useMemo<ITabs>(
    () => ({
      0: (
        <DescriptionTab
          description={SORTS_VALUES[sortName].description}
          gifPath={SORTS_VALUES[sortName].gifPath}
          title={SORTS_VALUES[sortName].title}
        />
      ),
      1: <PropertiesTab sortValues={SORTS_VALUES[sortName]} />,
      2: <SortingTab sortName={sortName} />,
    }),
    [sortName]
  );

  return (
    <details className={cls.sort} open={isOpen} onToggle={handleOpen}>
      <summary
        className={`${cls.sortHeader} ${index % 2 ? cls.odd : cls.even}`}
      >
        <Arrow className={`${cls.arrow} ${isOpen ? cls.rotateArrow : ''}`} />
        {SORTS_VALUES[sortName].title}
      </summary>
      <div className={cls.tabs}>
        {TABS_VALUES.map((values) => (
          <p
            key={values.key}
            className={`${cls.tab} ${
              values.number === activeTab ? cls.activeTab : ''
            }`}
            onClick={() => handleChangeTab(values.number)}
          >
            {values.title}
          </p>
        ))}
      </div>
      {tabs[activeTab]}
    </details>
  );
};
