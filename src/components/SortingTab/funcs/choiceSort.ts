import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import {
  setTwoIsSelect,
  setIsLastPosition,
  swapTwoElements,
  offTwoIsSelectCasive,
} from '../utils';

export const choiceSort = async ({
  array,
  isFromLower,
  setIsSorting,
  setSortedArray,
}: ICallFunctionProps) => {
  for (let i = 0; i < array.length; i++) {
    await setTwoIsSelect({ array, setSortedArray, ind1: i, ind2: i });
    let current = i;

    for (let j = i + 1; j < array.length; j++) {
      if (j === i + 1)
        await setTwoIsSelect({ array, setSortedArray, ind1: j, ind2: j });

      const isLast = j + 1 === array.length;
      if (
        isFromLower
          ? array[current].element > array[j].element
          : array[current].element < array[j].element
      ) {
        await offTwoIsSelectCasive({
          array,
          setSortedArray,
          ind1: current !== i ? current : isLast ? j : j + 1,
          ind2: isLast ? j : j + 1,
        });
        current = j;
      }

      if (current !== j)
        await offTwoIsSelectCasive({
          array,
          setSortedArray,
          ind1: j,
          ind2: isLast ? undefined : j + 1,
        });
    }

    if (current !== i)
      await swapTwoElements({ array, setSortedArray, ind1: i, ind2: current });

    await setIsLastPosition({ array, setSortedArray, ind: i, ind2: current });
  }

  setIsSorting(false);
};
