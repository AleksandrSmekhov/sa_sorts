import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import { fastSortStep } from '../subFuncs';

export const fastSort = async (props: ICallFunctionProps) => {
  await fastSortStep({ ...props, left: 0, right: props.array.length - 1 });
  props.setIsSorting(false);
};
