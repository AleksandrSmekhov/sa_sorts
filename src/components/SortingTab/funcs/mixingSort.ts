import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import {
  setTwoIsSelect,
  offTwoIsSelectCasive,
  setIsLastPosition,
  swapTwoElements,
} from '../utils';

export const mixingSort = async ({
  array,
  isFromLower,
  setIsSorting,
  setSortedArray,
}: ICallFunctionProps) => {
  let left = 0;
  let right = array.length - 1;

  while (left <= right) {
    for (let i = left; i < right; i++) {
      if (i === left)
        await setTwoIsSelect({ array, setSortedArray, ind1: i, ind2: i + 1 });

      if (
        isFromLower
          ? array[i + 1].element < array[i].element
          : array[i + 1].element > array[i].element
      ) {
        await swapTwoElements({ array, setSortedArray, ind1: i, ind2: i + 1 });
      }

      const isLast = i + 1 === right;
      await offTwoIsSelectCasive({
        array,
        setSortedArray,
        ind1: i,
        ind2: isLast ? undefined : i + 2,
      });
    }
    await setIsLastPosition({
      array,
      setSortedArray,
      ind: right,
    });

    --right;

    for (let i = right; i > left; i--) {
      if (i === right)
        await setTwoIsSelect({ array, setSortedArray, ind1: i, ind2: i - 1 });

      if (
        isFromLower
          ? array[i - 1].element > array[i].element
          : array[i - 1].element < array[i].element
      ) {
        await swapTwoElements({ array, setSortedArray, ind1: i - 1, ind2: i });
      }

      const isLast = i - 1 === left;
      await offTwoIsSelectCasive({
        array,
        setSortedArray,
        ind1: i,
        ind2: isLast ? undefined : i - 2,
      });
    }
    await setIsLastPosition({
      array,
      setSortedArray,
      ind: left,
    });

    ++left;
  }

  setIsSorting(false);
};
