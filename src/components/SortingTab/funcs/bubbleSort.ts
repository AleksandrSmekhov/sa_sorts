import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import {
  setTwoIsSelect,
  offTwoIsSelectCasive,
  setIsLastPosition,
  swapTwoElements,
} from '../utils';

export const bubbleSort = async ({
  array,
  isFromLower,
  setIsSorting,
  setSortedArray,
}: ICallFunctionProps) => {
  for (let i = 0; i + 1 < array.length; i++) {
    for (let j = 0; j + 1 < array.length - i; j++) {
      if (j === 0)
        await setTwoIsSelect({ array, setSortedArray, ind1: j, ind2: j + 1 });

      if (
        isFromLower
          ? array[j + 1].element < array[j].element
          : array[j + 1].element > array[j].element
      ) {
        await swapTwoElements({ array, setSortedArray, ind1: j, ind2: j + 1 });
      }

      const isLast = j + 2 === array.length - i;
      await offTwoIsSelectCasive({
        array,
        setSortedArray,
        ind1: j,
        ind2: isLast ? undefined : j + 2,
      });
    }
    await setIsLastPosition({
      array,
      setSortedArray,
      ind: array.length - i - 1,
    });
  }
  await setIsLastPosition({
    array,
    setSortedArray,
    ind: 0,
  });
  setIsSorting(false);
};
