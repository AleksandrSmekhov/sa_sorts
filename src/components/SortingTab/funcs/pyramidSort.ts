import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import {
  offTwoIsSelectCasive,
  swapTwoElements,
  setTwoIsSelect,
  setIsLastPosition,
} from '../utils';

import { heapify } from '../subFuncs';

export const pyramidSort = async ({
  array,
  isFromLower,
  setIsSorting,
  setSortedArray,
}: ICallFunctionProps) => {
  for (let i = Math.floor(array.length / 2) - 1; i >= 0; i--)
    await heapify({
      array,
      isFromLower,
      setSortedArray,
      heapSize: array.length,
      nodeInd: i,
    });

  for (let i = array.length - 1; i > 0; i--) {
    await setTwoIsSelect({ array, setSortedArray, ind1: 0, ind2: i });
    await swapTwoElements({ array, setSortedArray, ind1: 0, ind2: i });
    await setIsLastPosition({ array, setSortedArray, ind: i });
    await offTwoIsSelectCasive({ array, setSortedArray, ind1: 0 });

    await heapify({
      array,
      isFromLower,
      setSortedArray,
      heapSize: i,
      nodeInd: 0,
    });
  }
  await setIsLastPosition({ array, setSortedArray, ind: 0 });

  setIsSorting(false);
};
