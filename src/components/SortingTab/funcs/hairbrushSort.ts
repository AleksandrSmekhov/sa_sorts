import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import {
  setTwoIsSelect,
  offTwoIsSelectCasive,
  setIsLastPosition,
  swapTwoElements,
  changeTwoIsSelect,
} from '../utils';

const REDUCTION_FACTOR = 1.247;

export const hairbrushSort = async ({
  array,
  isFromLower,
  setIsSorting,
  setSortedArray,
}: ICallFunctionProps) => {
  let gap = array.length - 1;

  while (gap >= 1) {
    for (let i = 0; i + gap < array.length; i++) {
      if (i === 0)
        await setTwoIsSelect({ array, setSortedArray, ind1: i, ind2: i + gap });

      if (
        isFromLower
          ? array[i].element > array[i + gap].element
          : array[i].element < array[i + gap].element
      ) {
        await swapTwoElements({
          array,
          setSortedArray,
          ind1: i,
          ind2: i + gap,
        });
      }

      const isLast = i + gap === array.length - 1;
      if (gap === 1) {
        await offTwoIsSelectCasive({
          array,
          setSortedArray,
          ind1: i,
          ind2: isLast ? undefined : i + 2,
          withEnd: true,
        });
      } else {
        await changeTwoIsSelect({
          array,
          setSortedArray,
          ind1: i,
          ind3: i + gap,
          ind2: isLast ? undefined : i + 1,
          ind4: isLast ? undefined : i + gap + 1,
        });
      }
    }

    gap = Math.floor(gap / REDUCTION_FACTOR);

    if (!gap) {
      await setIsLastPosition({ array, setSortedArray, ind: array.length - 1 });
    }
  }

  setIsSorting(false);
};
