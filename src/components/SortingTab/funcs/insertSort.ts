import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import { setTwoIsSelect, setIsLastPosition, swapTwoElements } from '../utils';

export const insertSort = async ({
  array,
  isFromLower,
  setIsSorting,
  setSortedArray,
}: ICallFunctionProps) => {
  for (let i = 0; i < array.length; i++) {
    let j = i;
    await setTwoIsSelect({ array, setSortedArray, ind1: i, ind2: i });

    while (
      j > 0 &&
      (isFromLower
        ? array[j - 1].element > array[j].element
        : array[j - 1].element < array[j].element)
    ) {
      await swapTwoElements({
        array,
        setSortedArray,
        ind1: j - 1,
        ind2: j,
        isOne: true,
      });
      j--;
    }

    await setIsLastPosition({ array, setSortedArray, ind: j });
  }

  setIsSorting(false);
};
