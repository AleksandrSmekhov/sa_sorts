import type { ICallFunctionProps } from '../ui/SortingTab.interface';

import { expandArray } from '../utils';

import {
  divideArray,
  getFlatArray,
  mergeSortStep,
  getDividedArrayFromFlat,
} from '../subFuncs';

export const mergeSort = async ({
  array,
  setSortedArray,
  setIsSorting,
  isFromLower,
}: ICallFunctionProps) => {
  let dividedArray = await divideArray({ array, setSortedArray });

  while (dividedArray.length !== 1) {
    let flatArray = getFlatArray(dividedArray);
    let startInd = 0;

    for (let i = 0; i < dividedArray.length; i += 2) {
      if (i + 1 === dividedArray.length) break;

      flatArray = await expandArray({
        array1Length: dividedArray[i].length,
        array2Length: dividedArray[i + 1].length,
        array: flatArray,
        setSortedArray,
        startInd,
      });

      flatArray = await mergeSortStep({
        array: flatArray,
        setSortedArray,
        isFromLower,
        array1Length: dividedArray[i].length,
        array2Length: dividedArray[i + 1].length,
        startInd: startInd,
        isLast: dividedArray.length === 2 ? true : false,
      });

      startInd += dividedArray[i].length + dividedArray[i + 1].length + 1;
    }

    dividedArray = getDividedArrayFromFlat({ array: flatArray });
  }

  setIsSorting(false);
};
