import { useState } from 'react';

import { BinaryTree } from 'components/BinaryTree';

import type {
  ISortingTabProps,
  ISortedArrayItem,
} from './SortingTab.interface';

import { SORTS_FUNCTIONS } from '../const';

import cls from './SortingTab.module.scss';
import { SortNames } from 'components/SortingList/const';

export const SortingTab = ({ sortName }: ISortingTabProps) => {
  const [inputValue, setInputValue] = useState<string>('');
  const [array, setArray] = useState<number[]>([]);
  const [sortedArray, setSortedArray] = useState<ISortedArrayItem[]>([]);
  const [isSorting, setIsSorting] = useState<boolean>(false);
  const [isFromLower, setIsFromLower] = useState<boolean>(true);

  const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value) {
      setInputValue('');
      return;
    }

    if (!event.target.value.match(/^[\d ]*$/)) return;

    const newValue = event.target.value
      .split(' ')
      .map((value) => (value ? Number.parseInt(value) : ''));

    setInputValue(newValue.join(' '));

    setArray(newValue.filter((value): value is number => value !== ''));
  };

  const handleSort = () => {
    const newArray = array.map((element) => ({
      element,
      isEndPosition: false,
      isSelected: false,
      isSwitching: false,
    }));

    setIsSorting(true);
    setSortedArray(newArray);

    SORTS_FUNCTIONS[sortName]({
      array: newArray,
      setIsSorting,
      setSortedArray,
      isFromLower,
    });
  };

  const handleChangeSortDirection = () => {
    setIsFromLower((prev) => !prev);
  };

  return (
    <div className={cls.testing}>
      <p className={cls.hint}>Введите массив чисел через пробел:</p>
      <input
        autoComplete="off"
        className={`${cls.input} ${isSorting ? cls.disabledInput : ''}`}
        id={`${sortName}-arrayInput`}
        value={inputValue}
        onChange={handleChangeInput}
        disabled={isSorting}
      />
      {array.length ? (
        <>
          <h4 className={cls.title}>Введённый массив:</h4>
          <div className={cls.array}>
            {array.map((value, index) => (
              <p
                key={`${sortName}-input-array-element-${index}`}
                className={cls.arrayElement}
              >
                {value}
              </p>
            ))}
          </div>
          {array.length > 1 ? (
            <div className={cls.sortButtons}>
              <p>В порядке возрастания:</p>
              <label className={cls.checkBoxSwitch}>
                <input
                  type="checkbox"
                  checked={isFromLower}
                  onChange={handleChangeSortDirection}
                  disabled={isSorting}
                  id={`${sortName}-sort-direction`}
                  className={cls.checkbox}
                />
                <span
                  className={`${cls.checkBoxLabel} ${
                    isFromLower ? cls.checkBoxLabelFocus : ''
                  }`}
                />
              </label>
              <button
                className={`${cls.sortButton} ${
                  isSorting ? cls.disabledSortButton : ''
                }`}
                onClick={handleSort}
                disabled={isSorting}
              >
                {isSorting ? 'Сортировка...' : 'Сортировать'}
              </button>
            </div>
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
      {sortedArray.length ? (
        <>
          <h4 className={cls.title}>Отсортированный массив:</h4>
          <div className={cls.array}>
            {sortedArray.map((value, index) => (
              <p
                key={`${sortName}-sorted-array-element-${index}`}
                className={`${cls.arrayElement} ${
                  value.isEndPosition
                    ? cls.elementFinish
                    : value.isSwitching
                    ? cls.elementSwitch
                    : value.isSelected
                    ? cls.elementSelect
                    : ''
                } ${value.element === -1 ? cls.divider : ''}`}
              >
                {value.element < 0 ? '' : value.element}
              </p>
            ))}
          </div>
          {sortName === SortNames.pyramid ? (
            <>
              <h4 className={cls.title}>Бинарное дерево:</h4>
              <BinaryTree array={sortedArray} />
            </>
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
    </div>
  );
};
