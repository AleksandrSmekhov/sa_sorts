import { SortNames } from 'components/SortingList/const';

export interface ISortingTabProps {
  sortName: SortNames;
}

export interface ISortedArrayItem {
  element: number;
  isSelected: boolean;
  isSwitching: boolean;
  isEndPosition: boolean;
}

export interface ICallFunctionProps {
  array: ISortedArrayItem[];
  setSortedArray: React.Dispatch<React.SetStateAction<ISortedArrayItem[]>>;
  setIsSorting: React.Dispatch<React.SetStateAction<boolean>>;
  isFromLower: boolean;
}

export type ISortsFunctions = Record<
  SortNames,
  (props: ICallFunctionProps) => void
>;

export type ICallUtilFunctionProps = Omit<
  ICallFunctionProps,
  'isFromLower' | 'setIsSorting'
>;

export type ICallSubFunctionsProps = Omit<ICallFunctionProps, 'setIsSorting'>;
