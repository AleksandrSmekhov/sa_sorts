import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

import { DIVIDE_ELEMENT_WITH_BORDER, DIVIDE_ELEMENT } from '../const';

interface IExpandArray extends ICallUtilFunctionProps {
  startInd: number;
  array1Length: number;
  array2Length: number;
}

export const expandArray = async ({
  startInd,
  array1Length,
  array2Length,
  array,
  setSortedArray,
}: IExpandArray) => {
  array = [
    ...array.slice(0, startInd),
    ...Array.from({ length: array1Length + array2Length }, () => ({
      ...DIVIDE_ELEMENT_WITH_BORDER,
    })),
    { ...DIVIDE_ELEMENT },
    ...array.slice(startInd),
  ];

  await setWithTimer({ setSortedArray, newArray: [...array] });
  return array;
};
