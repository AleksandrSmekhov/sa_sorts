import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface ISetTwoIsSelectProps extends ICallUtilFunctionProps {
  ind1: number;
  ind2: number;
}

export const setTwoIsSelect = async ({
  array,
  setSortedArray,
  ind1,
  ind2,
}: ISetTwoIsSelectProps) => {
  array[ind1].isSelected = true;
  array[ind2].isSelected = true;

  await setWithTimer({
    setSortedArray,
    newArray: [...array],
  });
};
