import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface IOffThreeIsSelectProps extends ICallUtilFunctionProps {
  ind1: number;
  ind2: number;
  ind3: number;
}

export const offThreeIsSelect = async ({
  array,
  setSortedArray,
  ind1,
  ind2,
  ind3,
}: IOffThreeIsSelectProps) => {
  array[ind1].isSelected = false;
  array[ind2].isSelected = false;
  array[ind3].isSelected = false;

  await setWithTimer({
    setSortedArray,
    newArray: [...array],
  });
};
