import { ISortedArrayItem } from '../ui/SortingTab.interface';

import { TIMEOUT_DELAY } from '../const';

const timer = () => new Promise((res) => setTimeout(res, TIMEOUT_DELAY));

interface ISetWithTimerProps {
  setSortedArray: React.Dispatch<React.SetStateAction<ISortedArrayItem[]>>;
  newArray: ISortedArrayItem[];
}

export const setWithTimer = async ({
  newArray,
  setSortedArray,
}: ISetWithTimerProps) => {
  setSortedArray(newArray);
  await timer();
};
