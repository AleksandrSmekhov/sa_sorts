import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface IOffIsSwitchProps extends ICallUtilFunctionProps {
  ind1: number;
  ind2: number;
  isOne?: boolean;
}

export const swapTwoElements = async ({
  array,
  setSortedArray,
  ind1,
  ind2,
  isOne,
}: IOffIsSwitchProps) => {
  array[ind2].isSwitching = true;
  if (!isOne) array[ind1].isSwitching = true;

  await setWithTimer({ setSortedArray, newArray: [...array] });

  [array[ind1], array[ind2]] = [array[ind2], array[ind1]];
  array[ind1].isSwitching = false;
  if (!isOne) array[ind2].isSwitching = false;

  await setWithTimer({ setSortedArray, newArray: [...array] });
};
