import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface ISetIsLastPositionProps extends ICallUtilFunctionProps {
  ind: number;
  ind2?: number;
}

export const setIsLastPosition = async ({
  array,
  setSortedArray,
  ind,
  ind2,
}: ISetIsLastPositionProps) => {
  array[ind].isSelected = false;
  array[ind].isEndPosition = true;

  if (ind2) array[ind2].isSelected = false;

  await setWithTimer({
    setSortedArray,
    newArray: [...array],
  });
};
