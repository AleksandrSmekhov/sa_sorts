import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface ISkipElementsProps extends ICallUtilFunctionProps {
  startGapInd: number;
  endGapInd: number;
}

export const skipElements = async ({
  array,
  setSortedArray,
  startGapInd,
  endGapInd,
}: ISkipElementsProps) => {
  array = [...array.slice(0, startGapInd), ...array.slice(endGapInd + 1)];
  await setWithTimer({ setSortedArray, newArray: [...array] });
  return array;
};
