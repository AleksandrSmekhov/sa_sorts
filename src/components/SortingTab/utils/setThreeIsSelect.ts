import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface ISetThreeIsSelectProps extends ICallUtilFunctionProps {
  ind1: number;
  ind2: number;
  ind3: number;
}

export const setThreeIsSelect = async ({
  array,
  setSortedArray,
  ind1,
  ind2,
  ind3,
}: ISetThreeIsSelectProps) => {
  array[ind1].isSelected = true;
  array[ind2].isSelected = true;
  array[ind3].isSelected = true;

  await setWithTimer({
    setSortedArray,
    newArray: [...array],
  });
};
