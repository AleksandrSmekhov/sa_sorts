import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface IOffTwoIsSelectCasiveProps extends ICallUtilFunctionProps {
  ind1: number;
  ind2?: number;
  withEnd?: boolean;
}

export const offTwoIsSelectCasive = async ({
  array,
  setSortedArray,
  ind1,
  ind2,
  withEnd,
}: IOffTwoIsSelectCasiveProps) => {
  array[ind1].isSelected = withEnd ? true : false;
  if (withEnd) array[ind1].isEndPosition = true;

  if (ind2 || ind2 === 0) array[ind2].isSelected = true;

  await setWithTimer({
    setSortedArray,
    newArray: [...array],
  });
};
