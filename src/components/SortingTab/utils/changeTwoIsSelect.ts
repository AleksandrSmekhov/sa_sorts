import type { ICallUtilFunctionProps } from '../ui/SortingTab.interface';

import { setWithTimer } from './timer';

interface IChangeTwoIsSelect extends ICallUtilFunctionProps {
  ind1: number;
  ind3: number;
  ind2?: number;
  ind4?: number;
}

export const changeTwoIsSelect = async ({
  array,
  ind1,
  ind3,
  setSortedArray,
  ind2,
  ind4,
}: IChangeTwoIsSelect) => {
  if ((ind2 || ind2 === 0) && (ind4 || ind4 === 0)) {
    array[ind1].isSelected = false;
    array[ind3].isSelected = false;

    array[ind2].isSelected = true;
    array[ind4].isSelected = true;

    await setWithTimer({
      setSortedArray,
      newArray: [...array],
    });

    return;
  }

  array[ind1].isSelected = false;
  array[ind3].isSelected = false;

  await setWithTimer({
    setSortedArray,
    newArray: [...array],
  });
};
