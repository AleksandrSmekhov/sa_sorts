import {
  bubbleSort,
  mixingSort,
  hairbrushSort,
  insertSort,
  choiceSort,
  fastSort,
  mergeSort,
  pyramidSort,
} from './funcs';

import type {
  ISortsFunctions,
  ISortedArrayItem,
} from './ui/SortingTab.interface';

export const SORTS_FUNCTIONS: ISortsFunctions = {
  bubbleSort: bubbleSort,
  mixingSort: mixingSort,
  hairbrushSort: hairbrushSort,
  insertSort: insertSort,
  choiceSort: choiceSort,
  fastSort: fastSort,
  mergeSort: mergeSort,
  pyramidSort: pyramidSort,
};

export const TIMEOUT_DELAY = 250;

export const DIVIDE_ELEMENT: ISortedArrayItem = {
  element: -1,
  isEndPosition: false,
  isSelected: false,
  isSwitching: false,
};

export const DIVIDE_ELEMENT_WITH_BORDER: ISortedArrayItem = {
  element: -2,
  isEndPosition: false,
  isSelected: false,
  isSwitching: false,
};
