import type { ISortedArrayItem } from '../ui/SortingTab.interface';

interface IGetDividedArrayFromFlatProps {
  array: ISortedArrayItem[];
}

export const getDividedArrayFromFlat = ({
  array,
}: IGetDividedArrayFromFlatProps) => {
  let tempArray: ISortedArrayItem[] = [];
  const dividedArray: ISortedArrayItem[][] = [];

  for (let i = 0; i < array.length; i++) {
    if (array[i].element === -1) {
      dividedArray.push(tempArray);
      tempArray = [];
    } else {
      tempArray.push(array[i]);
    }
  }

  dividedArray.push(tempArray);

  return dividedArray;
};
