import type { ICallSubFunctionsProps } from '../ui/SortingTab.interface';

import {
  setThreeIsSelect,
  offTwoIsSelectCasive,
  offThreeIsSelect,
  swapTwoElements,
} from '../utils';

interface IHeapifyProps extends ICallSubFunctionsProps {
  heapSize: number;
  nodeInd: number;
}

export const heapify = async ({
  array,
  isFromLower,
  setSortedArray,
  heapSize,
  nodeInd,
}: IHeapifyProps) => {
  let largest = nodeInd;
  const [left, right] = [2 * nodeInd + 1, 2 * nodeInd + 2];

  await setThreeIsSelect({
    array,
    setSortedArray,
    ind1: largest,
    ind2: left < heapSize ? left : largest,
    ind3: right < heapSize ? right : largest,
  });

  if (
    left < heapSize &&
    (isFromLower
      ? array[left].element > array[largest].element
      : array[left].element < array[largest].element)
  ) {
    await offTwoIsSelectCasive({ array, setSortedArray, ind1: largest });
    largest = left;
  }

  if (
    right < heapSize &&
    (isFromLower
      ? array[right].element > array[largest].element
      : array[right].element < array[largest].element)
  ) {
    await offTwoIsSelectCasive({ array, setSortedArray, ind1: largest });
    largest = right;
  }

  if (largest !== nodeInd) {
    await swapTwoElements({
      array,
      setSortedArray,
      ind1: nodeInd,
      ind2: largest,
    });

    await offThreeIsSelect({
      array,
      setSortedArray,
      ind1: nodeInd,
      ind2: left < heapSize ? left : nodeInd,
      ind3: right < heapSize ? right : nodeInd,
    });

    await heapify({
      array,
      isFromLower,
      setSortedArray,
      heapSize: heapSize,
      nodeInd: largest,
    });
  } else {
    await offThreeIsSelect({
      array,
      setSortedArray,
      ind1: largest,
      ind2: left < heapSize ? left : largest,
      ind3: right < heapSize ? right : largest,
    });
  }
};
