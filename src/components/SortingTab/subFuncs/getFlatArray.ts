import type { ISortedArrayItem } from '../ui/SortingTab.interface';

import { DIVIDE_ELEMENT } from '../const';

export const getFlatArray = (array: ISortedArrayItem[][]) => {
  return array
    .map((arr, index) => {
      return index === array.length - 1
        ? [...arr]
        : [...arr, { ...DIVIDE_ELEMENT }];
    })
    .flat();
};
