import type { ICallSubFunctionsProps } from '../ui/SortingTab.interface';

import {
  swapTwoElements,
  changeTwoIsSelect,
  setIsLastPosition,
  setThreeIsSelect,
  skipElements,
} from '../utils';

interface IMergeSortStep extends ICallSubFunctionsProps {
  array1Length: number;
  array2Length: number;
  startInd: number;
  isLast: boolean;
}

export const mergeSortStep = async ({
  array,
  setSortedArray,
  array1Length,
  array2Length,
  startInd,
  isFromLower,
  isLast,
}: IMergeSortStep) => {
  let i1 = startInd + array1Length + array2Length + 1;
  let i2 = i1 + array1Length + 1;
  await setThreeIsSelect({
    setSortedArray,
    array,
    ind1: startInd,
    ind2: i1,
    ind3: i2,
  });

  const [i1End, i2End, startIndEnd] = [
    i1 + array1Length,
    i2 + array2Length,
    i1 - 1,
  ];

  while (i1 < i1End || i2 < i2End) {
    let isFirst;
    if (i1 === i1End) isFirst = false;
    else if (i2 === i2End) isFirst = true;
    else
      isFirst =
        array[i1].element > array[i2].element ? !isFromLower : isFromLower;

    await swapTwoElements({
      array: array,
      ind1: startInd,
      ind2: isFirst ? i1 : i2,
      setSortedArray,
    });

    const [currentInd, currentEnd] = isFirst ? [i1, i1End] : [i2, i2End];
    const isEnd = startInd + 1 === startIndEnd;

    await changeTwoIsSelect({
      array: array,
      ind1: startInd,
      ind3: currentInd,
      setSortedArray,
      ind2: isEnd ? undefined : startInd + 1,
      ind4: isEnd
        ? undefined
        : currentInd + 1 === currentEnd
        ? startInd + 1
        : currentInd + 1,
    });

    if (isLast)
      await setIsLastPosition({
        array: array,
        ind: startInd,
        setSortedArray,
      });

    isFirst ? i1++ : i2++;
    startInd++;
  }

  array = await skipElements({
    array,
    setSortedArray,
    startGapInd: startInd,
    endGapInd: startInd + array1Length + array2Length + 1,
  });

  return array;
};
