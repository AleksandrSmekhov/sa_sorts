import type {
  ICallUtilFunctionProps,
  ISortedArrayItem,
} from '../ui/SortingTab.interface';

import { setWithTimer } from '../utils/timer';
import { getFlatArray } from './getFlatArray';

interface IChuckArrayProps {
  array: ISortedArrayItem[];
  chunkSize: number;
}

const chunkArray = ({ array, chunkSize }: IChuckArrayProps) => {
  const numberOfChunks = Math.ceil(array.length / chunkSize);

  return [...Array(numberOfChunks)].map((_, index) =>
    array.slice(index * chunkSize, (index + 1) * chunkSize)
  );
};

interface IFlatArrayProps {
  array: ISortedArrayItem[][];
  setSortedArray: React.Dispatch<React.SetStateAction<ISortedArrayItem[]>>;
  chunkSize: number;
}

const flatArray = async ({
  array,
  setSortedArray,
  chunkSize,
}: IFlatArrayProps) => {
  const tempArray: ISortedArrayItem[][] = [];
  array.forEach((arr) => {
    tempArray.push(...chunkArray({ array: arr, chunkSize }));
  });
  array = tempArray;
  await setWithTimer({
    setSortedArray,
    newArray: getFlatArray(array),
  });
};

export const divideArray = async ({
  array,
  setSortedArray,
}: ICallUtilFunctionProps) => {
  let result: ISortedArrayItem[][] = [[...array]];
  for (let i = Math.ceil(array.length / 2); i > 1; i = Math.ceil(i / 2)) {
    const tempArray: ISortedArrayItem[][] = [];
    for (const arr of result) {
      tempArray.push(...chunkArray({ array: arr, chunkSize: i }));
      await flatArray({ array: tempArray, chunkSize: i, setSortedArray });
    }
  }

  await flatArray({ array: result, chunkSize: 1, setSortedArray });
  result = array.map((el) => [el]);
  return result;
};
