import type { ICallSubFunctionsProps } from '../ui/SortingTab.interface';

import { setIsLastPosition } from '../utils';

import { placeSupport } from './placeSupport';

interface IFastSortStepProps extends ICallSubFunctionsProps {
  left: number;
  right: number;
}

export const fastSortStep = async ({
  left,
  right,
  ...props
}: IFastSortStepProps) => {
  if (left < right) {
    const support = await placeSupport({ ...props, left, right });
    await fastSortStep({ ...props, left, right: support - 1 });
    await fastSortStep({ ...props, left: support + 1, right });
  }
  if (left === right) {
    setIsLastPosition({
      array: props.array,
      setSortedArray: props.setSortedArray,
      ind: left,
    });
  }
};
