import type { ICallSubFunctionsProps } from '../ui/SortingTab.interface';

import {
  setTwoIsSelect,
  offTwoIsSelectCasive,
  swapTwoElements,
  changeTwoIsSelect,
  setIsLastPosition,
} from '../utils';

interface IPlaceSupportProps extends ICallSubFunctionsProps {
  left: number;
  right: number;
}

export const placeSupport = async ({
  array,
  left,
  right,
  isFromLower,
  setSortedArray,
}: IPlaceSupportProps) => {
  await setTwoIsSelect({ array, setSortedArray, ind1: right, ind2: right });
  await setTwoIsSelect({ array, setSortedArray, ind1: left, ind2: right - 1 });

  let less = left;

  for (let i = right - 1; i > less; ) {
    while (
      isFromLower
        ? array[less].element < array[right].element
        : array[less].element > array[right].element
    ) {
      await offTwoIsSelectCasive({
        array,
        setSortedArray,
        ind1: less,
        ind2: less + 1,
      });
      less++;
    }

    if (i <= less) break;

    if (
      isFromLower
        ? array[i].element < array[right].element
        : array[i].element > array[right].element
    ) {
      await swapTwoElements({ array, setSortedArray, ind1: less, ind2: i });
      await changeTwoIsSelect({
        array,
        setSortedArray,
        ind1: less,
        ind3: i,
        ind2: less + 1,
        ind4: i > less + 1 ? i - 1 : less + 1,
      });
      less++;
    } else {
      await offTwoIsSelectCasive({
        array,
        setSortedArray,
        ind1: i,
        ind2: i - 1,
      });
      i--;
    }
  }

  if (
    less < right &&
    (isFromLower
      ? array[less].element > array[right].element
      : array[less].element < array[right].element)
  )
    await swapTwoElements({ array, setSortedArray, ind1: less, ind2: right });
  await setIsLastPosition({ array, setSortedArray, ind: less, ind2: right });
  return less;
};
