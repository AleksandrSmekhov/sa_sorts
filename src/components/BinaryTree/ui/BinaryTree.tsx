import type { IBinaryTreeProps, ITree } from './BinaryTree.interface';

import cls from './BinaryTree.module.scss';

export const BinaryTree = ({ array }: IBinaryTreeProps) => {
  const filtredTree = array.every((element) => element.isEndPosition)
    ? array
    : array.filter((element) => !element.isEndPosition);
  const tree = filtredTree.reduce<ITree>((result, element, index) => {
    const tempArray = result.length ? result[result.length - 1] : [];

    if (Math.log2(index + 1) % 1) tempArray.push(element);
    else result.push([element]);

    return result;
  }, []);

  return (
    <div className={cls.tree}>
      {tree.map((line, lineIndex) => (
        <div
          key={`Binary-tree-line-${lineIndex}`}
          className={cls.treeLine}
          style={{
            gridTemplateColumns: `repeat(${Math.pow(2, lineIndex)}, 1fr)`,
            width:
              Math.pow(2, tree.length - 1) * 50 +
              tree[tree.length - 1].length * 5,
          }}
        >
          {line.map((element, elementIndex) => (
            <p
              key={`Line-element-${elementIndex}`}
              className={`${cls.arrayElement} ${
                element.isEndPosition
                  ? cls.elementFinish
                  : element.isSwitching
                  ? cls.elementSwitch
                  : element.isSelected
                  ? cls.elementSelect
                  : ''
              }`}
            >
              {element.element}
            </p>
          ))}
        </div>
      ))}
    </div>
  );
};
