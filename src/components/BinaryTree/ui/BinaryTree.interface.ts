import type { ISortedArrayItem } from 'components/SortingTab/ui/SortingTab.interface';

export interface IBinaryTreeProps {
  array: ISortedArrayItem[];
}

export type ITree = ISortedArrayItem[][];
