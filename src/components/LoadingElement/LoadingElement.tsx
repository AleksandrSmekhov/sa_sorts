import { ReactComponent as Loading } from 'icons/loading.svg';

import cls from './LoadingElement.module.scss';

export const LoadingElement = ({ extraClass }: { extraClass?: string }) => {
  return <Loading className={`${cls.loading} ${extraClass || ''}`} />;
};
